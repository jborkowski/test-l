import akka.actor.{Actor, ActorLogging, ActorSystem, Props}

import scala.util.Random
import scalaz._
import Scalaz._


object GameApp extends App {
  val system = ActorSystem()
  val executor = system.dispatcher

  val gameActor = system.actorOf(GameActor.props)

  val config = Config(2,6)
  val p1 = Player("Adam")
  val p2 = Player("Tomek")

  gameActor ! StartGame(p1, p2, config)

  val throws = for {
    t <- Seq.fill(config.numberOfDice)(MakeThrow)
    p <- Seq(p1, p2)
  } yield t(p)

  val allMsg = Seq.fill(config.numberOfTour)(throws.distinct).flatten
  allMsg.foreach(gameActor ! _)

  //gameActor ! GameResult

}

class GameActor extends Actor with ActorLogging with RollUtil {
  def receive: Receive = startState

  def startState: Receive = {
    case StartGame(p1, p2, config) =>
      val initTable = Map(p1 -> 0, p2 -> 0)
      val newState = state(scoreTable = initTable, config = config)
      context.become(newState)
      log.info(s"Hello $p1 and $p2")
    case _ =>
      log.info("You must start game first!")
  }

  def state(round: Int = 1,
            tourScore: Map[Player, Int] = Map.empty,
            scoreTable: Map[Player, Int] = Map.empty,
            config: Config): Receive = {

    case MakeThrow(p) =>
      tourScore.get(p) match {
        case Some(_) =>
          log.info("You are already thrown")

        case None =>
          val thrownNumbers = Seq.fill(config.numberOfDice)(next)

          val msg = thrownNumbers.mkString(s"Player $p thrown: ", " + ", s". It is summary: ${thrownNumbers.sum}, Great!")
          log.info(msg)

          val newScore = tourScore + (p -> thrownNumbers.sum)

          if (newScore.size >= 2) {
            val res = getWinners(newScore).toMap
            val newScoreTable = scoreTable |+| res
            val nextRoundNumber = round + 1

            if (nextRoundNumber > config.numberOfTour) {
              context.become(endState(scoreTable))
              log.info(s"Game won Player: ${scoreTable.maxBy(_._2)._1}")
              self ! GameResult
            } else {
              val newState = state(round = nextRoundNumber, scoreTable = newScoreTable, config = config)
              context.become(newState)
            }

            val msg = newScore
              .map { case (player, score) => s"Player: $player reached $score points\n" }
              .mkString(s"End of the tour\n", "", "")

            log.info(msg)
          } else {
            val newState = state(round = round, tourScore = newScore, scoreTable = scoreTable, config = config)
            context.become(newState)
          }
      }

    case _ =>
      log.info("Unsupported Message")

  }

  def endState(scoreTable: Map[Player, Int]): Receive = {
    case GameResult =>
      log.info(s"Game is over\n")
      log.info(scoreTable.map { case (p,s) => s"Player: $p reached $s points\n" }.mkString)
    case Restart =>
      context.become(startState)
    case _ =>
      log.info(s"Game is over\n")
  }
}

object GameActor {
  def props = Props(classOf[GameActor])
}

case class Player(name: String) {
  override def toString = name
}

case class Config(numberOfDice: Int, numberOfTour: Int)

sealed trait Message
case class StartGame(playerOne: Player, playerTwo: Player, config: Config) extends Message
case class MakeThrow(player: Player) extends Message
case object GameResult extends Message
case object Restart extends Message

trait RollUtil {
  def next: Int = {
    val n = 6 - 1 + 1
    Random.nextInt(n) + 1
  }

  def getWinners(toursScore: Map[Player, Int]): Seq[(Player, Int)] = {
    toursScore
      .toList
      .sortWith(_._2 > _._2)
      .take(2) match {
        case (p1, s1) :: (p2, s2) :: Nil  if s1 == s2 => (p1, 1) :: (p2, 1) :: Nil
        case (p1, s1) :: (p2, s2) :: Nil  if s1 > s2 => (p1, 2) :: (p2, 0) :: Nil
        case (p1, s1) :: (p2, s2) :: Nil  if s1 < s2 => (p1, 0) :: (p2, 2) :: Nil
        case _ => throw new IllegalArgumentException("")
      }
  }
}
