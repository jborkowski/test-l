val `scala_2.11` = "2.11.11"

val `scala_2.12` = "2.12.2"

scalaVersion := `scala_2.12`

name := "test"

version := "1.0.0"


val `org.scalatest_scalatest` = "org.scalatest" %% "scalatest" % "3.0.1" % "test"

val `org.scalacheck_scalacheck` = "org.scalacheck" %% "scalacheck" % "1.13.4" % "test"

val `com.typesafe.scala-logging_scala-logging` = "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0"

val `ch.qos.logback_logback-classic` = "ch.qos.logback" % "logback-classic" % "1.2.3"

val akkaVersion = "2.4.16"

lazy val root = projectName("root", new File(".")).settings(
  libraryDependencies ++= Seq(
    "org.scalaz" %% "scalaz-core" % "7.2.13",
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
    `org.scalatest_scalatest`,
    `org.scalacheck_scalacheck`,
    `com.typesafe.scala-logging_scala-logging`,
    `ch.qos.logback_logback-classic`
  )
)

def projectName(name: String, file: File): Project = Project(name, file).settings(
)
